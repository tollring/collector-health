
import java.io.File;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * Simplest implementation of Health Telemetry Designed to have no dependencies,
 * and to be executable on Java 1.6
 *
 */
public class HealthApplication {

	static String ident = null;
	static String dataPath = null;
	static File dataFolder;

	public static void main(String[] args) throws Exception {

		if (args.length < 2) {
			throw new Exception("Invalid command line; Usage: java HealthApplication {id} {collector folder}");
		}

		ident = args[0];
		dataPath = args[1];

		new HealthApplication().setup().runOnce();

	}

	/**
	 * @return
	 * @throws Exception
	 */
	HealthApplication setup() throws Exception {
		dataFolder = new File(dataPath);
		if (!dataFolder.exists()) {
			throw new Exception("No folder exists");
		}

		if (!dataFolder.isDirectory()) {
			throw new Exception("Invalid folder specified");
		}

		return this;
	}

	/**
	 * @throws Exception
	 */
	void runOnce() throws Exception {
		String healthData = collect();
    	push(healthData);
	}

	/**
	 * @throws Exception
	 * 
	 *                   void startSchedule() throws Exception { TimerTask task =
	 *                   new TimerTask() { public void run() {
	 *                   System.out.println("Poll: " + new Date() );
	 * 
	 *                   try {
	 * 
	 *                   runOnce();
	 * 
	 *                   }catch (Exception e) { e.printStackTrace(); } } }; Timer
	 *                   timer = new Timer("HealthTimer");
	 * 
	 *                   long delay = 1000L; long period = 60 * 1000L;
	 *                   timer.scheduleAtFixedRate(task, delay, period); }
	 */

	/**
	 * 
	 * @return Health data in JSON format
	 */
	String collect() {
		File[] files = dataFolder.listFiles();
		Integer fileCount = files.length;
		Date oldestFileCreationDate = null;
		File oldestFile = null;
		Date latestFileCreationDate = null;
		File latestFile = null;
		if (fileCount > 0) {
			long oldestDate = Long.MAX_VALUE;
			long latestDate = Long.MIN_VALUE;
			for (File f : files) {
				if (!f.getName().equalsIgnoreCase("desktop.ini")) {
					if (f.lastModified() < oldestDate) {
						oldestDate = f.lastModified();
						oldestFile = f;
					}
					if (f.lastModified() > latestDate)
		            {
						latestFile = f;
						latestDate = f.lastModified();
		            }
				}
			}
			oldestFileCreationDate = new Date(oldestDate);
			latestFileCreationDate = new Date(latestDate);
		}
		return new String("{\"ident\":\"" + ident + "\",\"fileCount\":\"" + fileCount + "\",\"oldestFile\":\""
				+ oldestFile.getName() + "\",\"oldestFileLMDate\":\"" + oldestFileCreationDate + "\",\"latestFile\":\""
				+ latestFile.getName() +"\",\"latestFileLMDate\":\"" + latestFileCreationDate + "\"}");
	}

	/**
	 * 
	 * @param healthData
	 * @throws Exception
	 */
	void push(String healthData) throws Exception {
		URL url = new URL("https://admin.icsuite.co.uk/ic360/health");
		URLConnection con = url.openConnection();
		HttpURLConnection http = (HttpURLConnection) con;
		http.setRequestMethod("POST");
		http.setDoOutput(true);

		byte[] out = healthData.getBytes(StandardCharsets.UTF_8);
		int length = out.length;

		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		http.connect();
		try (OutputStream os = http.getOutputStream()) {
			os.write(out);
		}
	}

}
